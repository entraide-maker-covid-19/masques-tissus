---
layout: page
title: Autres tissus
permalink: /tissus/autres
background: /medias/noun_Fabric_2806653.custom.svg
nav:
---

## les blouses et sur-blouses

La coordination, faire ou demander :

 - [fabricommuns.org](https://fabricommuns.org/)
 - [facebook.com / maskattack](https://www.facebook.com/groups/maskattack/)
 - [facebook.com / Couturières solidaires de France](https://www.facebook.com/groups/245595156478044/learning_content/)
 - [faisuneblouse.com](https://www.faisuneblouse.com/)
 - [coutureethopital.com](https://www.coutureethopital.com/)

Les patrons : 

 - [citronille.fr - Surblouse "Marie Agnès"](https://www.citronille.fr/fr/patrons-couture/851-surblouse-marie-agnes.html)
 - [csfmodeluxe-masques.com](csfmodeluxe-masques.com, patrons, normes de référence...)
 - [Préfécture de la Marne, patron](http://www.marne.gouv.fr/content/download/26673/168678/file/Patron_blouse_hospitaliere.pdf)
 - [Mask Attack : Les SURBLOUSES: V1+ V2](https://cdn.discordapp.com/attachments/691301124996857896/699627980477169785/SURBLOUSE_V2_-_MASK_ATTACK_-PDF.pdf)

## Charlottes et calots de bloc

 - [troizetoiles.fr - calots de bloc](http://www.troizetoiles.fr/tuto-calots-de-bloc-pour-femme/)
 - [maison-fauve.com - calot Derek](https://www.maison-fauve.com/pdf/pdf-femme/le-calot-derek-patron-gratuit)
 - [youtube/Biba Zary - bonnet medical](https://www.youtube.com/watch?v=he5swqc4BB4)


{% include_relative _content/tissus-documents-notices.md %}
