---
layout: page
title: Les tissus
permalink: /tissus/
background: 
nav: 1
index: 1
---


<div class="autogrid has-gutter txt-center">

<div>
#### les masques tissus

<span class="thumb thumb-h200">![](/medias/lepetitplus-1.jpg)</span>   

[Voir la page dédiée](/tissus/les-masques)
</div>

<div>
#### Autres productions

<!-- <span class="thumb thumb-h200">![](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/images/leplus.JPG)</span>-->   

 - [les blouses, sur-blouses, Charlottes, calots de bloc (...)](/tissus/autres)
 - [les notices et autres documents](/tissus/documents)
</div>

<div>
#### Tips &amp; astuces

 - [la FAQ sur les masques en tissu](/tissus/faq)
 - [Quelques solutions pour remplacer les élastiques](/tissus/quelques-solutions-pour-remplacer-les-elastiques)

</div>
</div>


