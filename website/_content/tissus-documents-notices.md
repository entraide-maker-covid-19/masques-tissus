

## les notices

L'objectif de ses notices générique et de fournir une base de travail. Elles sont sous licence cc0, vous pouvez les recopier, les modifier ...

<div class="autogrid has-gutter txt-center">

<div>
#### Notice utilisateur
Pour l'utilisateur de masque, notice générique convenant à la majorité des modèles de masque en tissus

[Télécharger la notice - pdf](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/notices/notice-utilisateur.pdf)
</div>

<div>
#### Fabrication pour particuliers et couturiers
Notice adressée aux particuliers et couturiers.

[Télécharger la notice - pdf](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/notices/notice-fabrication-et-distribution-couturiers.pdf)
</div>

<div>
#### Fabrication pour FabLab
Notice adressée aux FabLab et structures produisant en grand volume.

[Télécharger la notice - pdf](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/notices/notice-fabrication-et-distribution-en-fablab.pdf)
</div>

<div>
#### Livraison et réception
La notice pour la distribution et la réception de masques.

[Télécharger la notice - pdf](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/notices/notice-reception.pdf)
</div>
</div>

### Tips fabrication

 - [Quelques solutions pour remplacer les élastiques](/tissus/quelques-solutions-pour-remplacer-les-elastiques/)

### Visuels

<div class="autogrid has-gutter txt-center">
<div>
#### Masques tissus et COVID-19
![Preview](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/flyers/masque-general/flyer-preview.jpg)

[Télécharger](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/flyers/masque-general/flyer.png?inline=false)    
[Télécharger - imprimable](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/flyers/masque-general/print.png?inline=false)
</div>

<div>
#### Choix des tissus
![Preview](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/flyers/test-tissus/flyer-preview.jpg)

[Télécharger](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/flyers/test-tissus/flyer.png?inline=false)
</div>
</div>

### Documentations

 - [AFNOR - masque barrière](https://telechargement-afnor.com/masques-barrieres)    
   Specification AFNOR S76-001
 - [ifth.org / base de donnees des choix textiles](https://www.ifth.org/2020/03/30/covid-19-publication-de-la-base-de-donnees-avec-caracterisation-matiere-pour-la-realisation-de-masques-de-protection/)

