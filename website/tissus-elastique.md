---
layout: page
title: Quelques solutions pour remplacer les élastiques
permalink: /tissus/quelques-solutions-pour-remplacer-les-elastiques
background: /medias/noun_mask_3184746.custom.svg
nav: 
---

## Avant propos

Dans la situation actuelle, l'approvisionnement en élastique peut vite devenir un problème, nous avons réuni quelques solutions pour palier à ce problème.

## Les solutions présentées

 - Récupérer des élastiques
 - Economiser l’élastique
 - Utiliser des bandes de collants
 - Utiliser des bandes de tissu extensible, type jersey
 - Utiliser des bandes de tissu non extensible

## Téléchargement

[Télécharger le document - PDF](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/documentations/alternatives_elastiques.pdf?inline=false)
