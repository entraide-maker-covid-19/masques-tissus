---
layout: page
title: Foire Aux Questions
permalink: /tissus/faq
background: /medias/noun_mask_3184746.custom.svg
nav: 
---

### Est-ce qu'un masque en tissu me protège ? 

Le masque en tissu n'est pas fait pour se protèger, il protège les autres.
C’est un masque antiprojection : il évite la projection vers l’entourage des gouttelettes émises par celui qui porte le masque, à condition que le masque soit porté correctement. 

### Si le masque en tissu n'est pas fait pour me protéger, à quoi sert-il ?

Quand vous portez un masque, vous protégez les autres. Quand les autres en portent un, ils vous protègent.
Donc si tout le monde en porte, et que dans le même temps on respecte les gestes barrières et les règles de distanciation sociale, on ralentit la propagation du virus. 

### Si je ne suis pas malade, est-il utile que je porte un masque ?

Oui, c'est utile. Vous pouvez porter le virus et le transmettre, même si vous n'avez aucun symptome (porteur asymptomatique).

*voir la partie [Contagiosité de l'article Wikipédia consacré au Covid-19](https://fr.wikipedia.org/wiki/Maladie_%C3%A0_coronavirus_2019#Contagiosit%C3%A9)*

### Est-ce qu'avec un masque, je dois continuer d'appliquer les gestes barrières ?

Oui absolument, le masque n'est qu'un complément aux gestes barrières et aux régles de distanciation sociale.

### Le masque en tissu est-il aussi efficace que le masque chirugical ?

Le masque en tissu et le masque chirurgical sont deux masques antiprojections mais ils n'ont pas le même niveau de filtration. Les masques chirurgicaux filtrent mieux les gouttelettes (du porteur vers l'extérieur) que les masques en tissu (voir tableau ci-dessous).
En période de pénurie, les masques chirurgicaux restent réservés au personnel soignant.

![](/medias/les-differents-masques.jpg)

### Un masque en tissu du commerce est-il plus efficace qu'un masque en tissu fait maison?
Acheter un masque en tissu ne vous garantit pas son efficacité.
Attention, les arnaques fleurissent ! Si vous voulez être certain de la qualité du modèle, de son niveau de filtration et de respirabilité, il faut que le masque soit clairement identifié comme un masque de la catégorie 1 ou 2 des masques "Grand Public" ou Masques à Usage Non Sanitaire (UNS). Cela signifie que la matière a passée avec succès les tests de la DGA.
A noter qu'il n'existe à ce jour aucune norme AFNOR pour les masques en tissu, seulement des recommandations.

*voir le [tableau des producteurs et aux résultats des tests](https://www.entreprises.gouv.fr/files/files/home/Masques_alternatifs.pdf) disponible sur [le site entreprises.gouv.fr](
https://www.entreprises.gouv.fr/covid-19/liste-des-tests-masques-de-protection)*

### Comment choisir mon masque tissu ?

Il existe tellement de modèles et tellement de compositions textiles qu'il est très difficile de répondre à cette question. 
Quelques points de repère :
- Eviter les masques en tissu cousu avec une seule couche de tissu
- Eviter les masques en tissu avec une couture verticale au milieu devant le nez et la bouche
- Privilégier les masques en tissu accompagnés d'une notice d'utilisation bien claire
- Vérifier que le nom du fabriquant est clairement mentionné. Vous saurez à qui vous adresser en cas de problème.
- Vérifier la qualité des coutures, notamment au niveau des liens ou élastiques
- Si vous le pouvez, achetez un premier masque que vous testerez et retournez acheter les autres s'il vous convient. 
Vous testerez en particulier
    1. la stabilité du masque : vous ne devez pas avoir à le réajuster si vous bougez, si vous baillez, si vous riez...
    2. La respirabilité : vous devez pouvoir monter un escalier quasiment comme vous le feriez sans masque
    3. Son ajustement : si le masque baille à différents endroits et s'ajuste mal à votre visage, il ne jouera pas son rôle antiprojections. Certains masques existent en plusieurs tailles, il faut choisir la taille qui vous convient. Pour les masques taille unique, il faut pouvoir ajuster le masque à votre morphologie. Privilégiez les modèles avec pince-nez.
    4. Le confort : absence d'irritation au niveau des coutures, des élastiques. Vous l'oubliez presque quand vous le portez.

### Combien ca coûte ?

De zero (don) à 10 euros environ pièce.    
Penser à bien vérifier le nombre de lavages possibles. En effet, certains masques en tissu sont fabriqués dans des matières "intissé". Ces matières, qui filtrent bien et offrent une bonne respirabilité, ne supportent que quelques lavages.
Le budget peut vite s'avérer important si vous utilisez de nombreux masques (Rappel : le masque en tissu doit être lavé après chaque utilisation).

### comment nettoyer / désinfecter mon masque ?

 - PAS DE JAVEL (car toxique à respirer et risque de brûlure chimique)
 - en machine avec lessive, 60°c minimum 30 minutes
 - en l'absence de machine à laver, dans l'eau bouillante pendant 30 minutes (attention aux brûlures)


### A ne pas faire pour nettoyer / désinfecter son masque ?
 - n'utilisez pas de la javel (dangereux pour la santé)
 - n'utilisez pas un fer à repasser seul (ne suffit pas)
 - n'utilisez pas un séche cheveux seul (ne suffit pas)
 - n'utilisez pas votre four (risque d'incendie)
 - ne laissez pas laisser trainer votre masque un certain temps pour tuer le virus (attention aux bactèries)

