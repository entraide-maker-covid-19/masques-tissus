---
layout: page
title: Masque tissus
permalink: /tissus/les-masques
background: /medias/noun_mask_3184746.custom.svg
nav: 
---

## Avant propos

Un masque en tissu n’est pas un dispositif médical et n’est qu’un complément aux gestes
barrières.

Beaucoup de sites, groupes facebook, organismes, entreprises (...) proposent des modèles de masque, de choix de matériaux et se solutions.    
Nous ne cherchons pas à valider ou à tester les modèles, nous ne fournissons qu'un pack d'élèments pouvant être utilisé dans sa globalité ou partiellement.    

## Ressources

Nous mettons à votre disposition plusieurs modèles de masque en tissus (patrons et pas à pas), des notices d'utilisation, de fabrication (...) ainsi que d'autres ressources pour vous aider.

Vous pouvez télécharger l'intégralité de nos contenus <span class="button">[en cliquant ici](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/archive/master/masques-tissus-master.zip)</span>

## Les modèles

<div class="autogrid has-gutter txt-center">

<div>
#### Le petit plus
<span class="thumb thumb-h200">![](/medias/lepetitplus-1.jpg)</span>   
[Télécharger le modèle](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/modeles/FannyBarme/modele.pdf?inline=false)   
[Télécharger le patron 1](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/modeles/FannyBarme/patron-1_2.pdf?inline=false)   
[Télécharger le patron 2](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/modeles/FannyBarme/patron-2_2.pdf?inline=false)    
[Télécharger la notice](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/modeles/FannyBarme/notice-utilisateur.pdf?inline=false)      
[Télécharger le bon de remise](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/modeles/FannyBarme/bon-de-remise.pdf?inline=false)

[Télécharger l'ensemble](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/modeles/FannyBarme/le-petit-plus.zip?inline=false)   
</div>

<div>
#### FabLab de Bordeaux
<span class="thumb thumb-h200">![](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/images/sscout.JPG)</span>    
[Télécharger le modèle](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/modeles/FabLab-Bordeaux/patron-v1.7.pdf?inline=false)    
[Télécharger la vidéo](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/modeles/FabLab-Bordeaux/video.mp4?inline=false)
</div>

</div>

{% include_relative _content/tissus-documents-notices.md %}

