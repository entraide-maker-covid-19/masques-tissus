# Masques Tissus

Projet pour la gestion des fichiers liés à la création de masques tissus.

## Le petit plus

Masque avec couture. Prends en compte les recommandations de l'AFNOR.

![leplus](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/images/leplus.JPG)

## Sans couture

Modèle sans couture par le fablab de bordeaux

![sscout](https://gitlab.com/entraide-maker-covid-19/masques-tissus/-/raw/master/images/sscout.JPG)



## Documentation officielle

L'AFNOR a sorti un document sur les masques barrières, résumant les design à utiliser, l'entretien des masques, comment les porter, les mettre, etc.

[AFNOR : telechargement-afnor.org/masques-barrieres](https://telechargement-afnor.org/masques-barrieres)
Vous pouvez en trouver une copie dans le dossier documentation/
