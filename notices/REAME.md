# Les différentes notices disponible

Toutes les notices sont publiées sous la licence CC0 1.0 universel

## Notice : fabrication-et-distribution-en-fablab  
Cette notice s'adresse aux fablab.
Elle reprends les élèments clés de la documentation AFNOR (SPEC S76-001).
Pour produire en série, veuillez utiliser en complément la documentation AFNOR SPEC S76-001.

## Notice : fabrication-et-distribution-pour-les-courturier(e)s
Cette notice s'adresse aux couturier(e)s amateurs ou professionnels.
Elle reprends les élèments clés de la documentation AFNOR (SPEC S76-001).

## Notice : reception
Cette notice s'adresse aux personnes et orgarnismes réceptionnant des masques tissus.

## Notice : utilisateur
Cette notice s'adresse aux utilisateurs du masque.
Nous recommandons que cette notice doit distribuer à chaque utilisateur.
