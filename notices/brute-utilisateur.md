note : non corrigé


Un masque en tissu n’est pas un dispositif médical et ce n’est qu’un complément aux gestes barrière
 
Il ne faut pas retirer son masque lors des interactions sociales
 
Ce dispositif n’est ni un dispositif médical au sens du règlement UE/2017/745 (masques chirurgicaux), ni un équipement de protection individuelle au sens du Règlement UE/2016/425 (masques filtrants de type FFP2).


**Mettre le masque**

    1. Se laver les mains à l’eau et au savon ou utiliser une solution hydroalcoolique
    2. Placer son masque, en le manipulant par les élastiques
        a. en commençant par le haut (au niveau du nez)
        b. puis en en le glissant sous le menton (bien le couvrir)
    3. Enfin, pincer la barette nasale, si il y en a une


**Retirer le masque**

    1. Se laver les mains à l’eau et au savon ou utiliser une solution hydroalcoolique
    2. En le manipulant par les élastiques, retirer son masque sans se toucher le visage
    3. Ranger le masque dans un contenant dédié (aux masques sales)
    4. Se re-laver les mains


**Bonne utilisation du masque**

    - Conserver le masque propre dans un contenant dédié aux masques propres
    - Ne pas porter le masque plus de 3 heures
    - Changer le masque si il est humide
    - Se laver les mains avant de réajuster son masque ou de toucher son visage
    - Ne pas le partager (à usage personnel)
    - Laver le masque avant réutilisation


**à respecter**

    - Bien positionner le masque sur son visage
    - Ne pas toucher ou déplacer le masque aprés l’avoir positionné
    - Ne pas porter un masque mouillé ou souillé
    - Continuer de respecter les gestes barrières
    - Durée d’utilisation maximale : 3h pour un masque initialement propre
    - Ne convient pour la protection vis-à-vis des produits chimiques


**Comment nettoyer le masque ?**

    - machine 60°C 30 minutes minimum, avec lessive
    - casserole bain eau bouillante 30m

La lessive n’est pas obligatoire avec ses méthodes.  
ATTENTION : Avant de réutiliser un masque, il faudra vérifier son état attentivement



Ces masques ne possèdent aucune certification. Leur utilisation peut cependant être bénefique. Les informations ci dessus sont fournies pour la meilleure information de chacun. Cependant elles ne peuvent se substituer à la prudence, aux textes en vigueur ou aux meilleures pratiques sanitaires. La responsabilité de l'auteur de cette notice ne saurait être engagée.


Cette notice est publiée sous la licence CC0 1.0 universel
