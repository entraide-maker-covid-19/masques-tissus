note : non corrigé


l'AFNOR édite une spécification pour la fabrication en série et la confection artisanale (ref : AFNOR SPEC S76-001). La notice ci dessous se limite à la confection artisanale.

**partie réception**
 
 - Laver les tissus réceptionnés selon les recommandations de lavage


**partie fabrication**

- Avant la production
    - Si on travaille dans un espace partagé
        - nettoyer machines et plans de travail
             - eau savonneuse
             - éthanol 90°C
         - porter un masque
         - se laver les mains
    - Si on travaille à plusieurs
         - porter un masque
         - respecter les gestes barrières
         - se laver les mains


**Recommandations AFNOR pour la production artisanale**

- Utiliser des étoffes serrées, lisses et non irritantes
- Assembler en deux ou trois couches
- Utiliser des étoffes suffisamment souples pour s’appliquer autour du visage pour assurer l’étanchéité
- Utiliser des étoffes pas trop chaudes
- Ne pas utiliser d’agrafe dans la conception du masque barrière
- Ne pas faire de coutures verticales (allant du nez jusqu'au menton)



 **Après la production**
- Laver/desinfecter les masques produit
- Laver/desinfecter les emballages des masques
- Ensacher/empaqueter les masques individuellement ou par petits lots
    - Inclure la procédure de traçabilité, soit par masque, soit par lot (a minima nom du fabricant, moyen de contact et date de fabrication)


**avant distribution**

 - Regrouper les masques (carton/sac poubelle) par établissement/demande
 - Insérer les notices dans les emballages par établissement/demande
 - Préparer un ensemble de notices accessible sans devoir ouvrir l'emballage
 - Respecter les gestes barrière lors de la distribution


Cette notice est publiée sous la licence CC0 1.0 universel
