note : non corrigé


**Impératifs**

- Respecter les gestes barrières 
- Consulter les documents joints à la livraison
- Ne pas ouvrir les kits avant distribution
- 1 kit = 1 personne = 1 jour
- Se laver les mains avant déballage
- Consulter la notice utilisateur
 
**Recommandations**

- Conserver la fiche tracabilité
- Distribuer les kits proportionnellement aux besoins
- Lors de la distribution, rappeler : 
    - L'importance des gestes barrière
    - La responsabilité individuelle de chacun
    - Ne pas mélanger les masques propres et les masques utilisés 
    - Laver les masques tous les jours
