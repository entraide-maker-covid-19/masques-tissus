note : non corrigé


Notice pour les couturières, les couturiers ou particuliers qui confectionnent des masques en tissus à leur domicile
Confection artisanale (DIY) des masques barrières conformément aux recommandations de l’AFNOR (S76-001:2020) du 27 mars 2020


Avertissement :
Le masque en tissu, appelé "masque barrière" est destiné à compléter les gestes barrières et les règles de distanciation sociale. Il est destiné au grand public et non aux personnels soignants au contact des patients.  
Les masques barrières contribuent à limiter les risques de contamination, si l’on respecte des conditions pour la conception, la confection, la distribution et l’utilisation. Si ces conditions ne sont pas remplies, ils peuvent devenir un vecteur de contamination. Il est donc essentiel que vous respectiez les consignes ci-dessous.

Précautions à prendre de la réception des matières à la remise des masques :
•	Stopper la fabrication de masques si une personne de votre foyer présente des symptômes du Covid 19. 
•	Respecter les gestes barrières et les règles de distanciation sociale au sein de votre foyer.
•	Laver immédiatement et systématiquement tous les textiles entrant dans votre foyer à 60°C minimum pendant 90 minutes, avec une lessive classique. Une fois secs, les ranger dans un sac propre (sac en tissu, sac poubelle).
•	Nettoyer une fois par jour vos outils et machines à l’aide de lingettes désinfectantes ou d’eau savonneuse. Veillez à maintenir votre machine à laver propre (rinçage régulier à froid avec javel ou faire tourner à vide à 90°C), veiller à bien nettoyer les filtres de votre sèche-linge en vous lavant les mains après.
•	Lavez-vous les mains avant de commencer une séance de couture et après chaque pause.
•	Laver systématiquement les masques cousus en machine à 60°C minimum, pendant 90 minutes, avec une lessive classique. Les faire sécher au sèche-linge, sur un séchoir désinfecté au préalable ou sur un linge propre.
•	Une fois secs, les ranger immédiatement dans un contenant dont ils ne sortiront plus jusqu’à utilisation (sac plastique type sac congélation, sac en tissu, boîte en plastique), en fonction de leur utilisation future (à l’unité, par deux, par trois...). 
•	Coller une étiquette sur le contenant et y inscrire « Masque filtrant, fait par Untel, adresse ou mail, date, sur tel modèle »
•	Principe « emballé, pas toucher » : A partir de ce moment et jusqu’à l’instant de son placement sur le visage du porteur, le masque ne doit plus être touché. 
•	Principe « livraison sans contact » : Quels que soient les destinataires de vos masques (individu de votre entourage à l’extérieur de votre foyer, association, entreprises...), ils devront être livrés « sans contact » et toujours accompagnés d’une notice d’utilisation.
•	Si vous avez cousu ces masques pour une structure, avant de vous lancer dans la fabrication, demandez les consignes de conditionnement des masques et la procédure de livraison. Respecter ces consignes à la lettre. Si aucune procédure n’existe, mettez-vous d’accord avec la structure pour respecter les deux principes « emballé, pas toucher » et « livraison sans contact »
•	N’hésitez pas à insister auprès des personnes à qui vous donner vos masques sur les limites de leur efficacité et les conditions de leur utilisation.
