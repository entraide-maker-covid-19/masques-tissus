# Les différentes notices disponible

Toutes les notices sont publiées sous la licence CC0 1.0 universel

Les notices sont rédigées pour la production de masques en tissus, elles sont modifiable et adaptable à vos besoins.

Ce masque n’est ni un dispositif médical ( au sens du règlement UE/2017/745
masques chirurgicaux) , ni un équipement de protection individuelle ( au sens
du Règlement UE/2016/425 masques filtrants de type FFP2) . Il ne convient
pour la protection vis-à-vis des produits chimiques. Le masque en tissu “Le
Petit Plus“ n’est ni un Masque Barrière, ni un Masque Grand Public
(catégories 1 ou 2) ou Alternatif (catégories 1 ou 2).


## Notice : fabrication-et-distribution-en-fablab  
Cette notice s'adresse aux fablab.

## Notice : fabrication-et-distribution-pour-les-courturier(e)s
Cette notice s'adresse aux couturier(e)s amateurs ou professionnels.

## Notice : reception
Cette notice s'adresse aux personnes et orgarnismes réceptionnant des masques tissus.

## Notice : utilisateur
Cette notice s'adresse aux utilisateurs du masque.
Nous recommandons que cette notice doit distribuer à chaque utilisateur.
